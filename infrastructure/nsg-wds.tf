resource "azurerm_network_security_group" "nsg-wds" {
    name                = "nsg-${var.locale-short}-wds-${var.environment}"
    location            = "${var.locale}"
    resource_group_name = "${azurerm_resource_group.main.name}"

    security_rule {
      name                       = "LOOPBACK"
      priority                   = 101
      direction                  = "Inbound"
      access                     = "Allow"
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_range     = "*"
      source_address_prefix      = "172.${var.address-middle}.${var.wds-address-end}"
      destination_address_prefix = "*"
    }
    security_rule {
      name                       = "BAS_IN"
      priority                   = 102
      direction                  = "Inbound"
      access                     = "Allow"
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_range    = "${var.wds-port-102}"
      source_address_prefix      = "172.${var.address-middle}.${var.bas-address-end}"
      destination_address_prefix = "*"
    }
    security_rule {
      name                       = "EDGE_IN"
      priority                   = 103
      direction                  = "Inbound"
      access                     = "Allow"
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_ranges    = ["${var.wds-port-103}"]
      source_address_prefix      = "172.${var.address-middle}.${var.edge-address-end}"
      destination_address_prefix = "*"
    }
    security_rule {
      name                       = "MONITORING_IN"
      priority                   = 104
      direction                  = "Inbound"
      access                     = "Allow"
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_ranges    = ["${var.wds-port-104}"]
      source_address_prefix      = "${var.wds-source-104}"
      destination_address_prefix = "*"
    }
    security_rule {
      name                       = "OCTOPUS_IN"
      priority                   = 105
      direction                  = "Inbound"
      access                     = "Allow"
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_range     = "${var.wds-port-105}"
      source_address_prefix      = "${var.wds-source-105}"
      destination_address_prefix = "*"
    }
    security_rule {
      name                       = "DENY_ALL"
      priority                   = 4096
      direction                  = "Inbound"
      access                     = "Deny"
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_range     = "*"
      source_address_prefix      = "*"
      destination_address_prefix = "*"
    }
}
