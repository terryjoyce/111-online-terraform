resource "azurerm_lb" "intlb" {
  name                = "111-${var.locale-short}-${var.environment-short}-lb-int"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  frontend_ip_configuration {
    name                 = "111-${var.locale-short}-${var.environment-short}-lb-int-ip"
    subnet_id = "${azurerm_subnet.edge.id}"
    private_ip_address_allocation = "dynamic"
  }
}

resource "azurerm_lb_backend_address_pool" "intlb" {
  resource_group_name = "${azurerm_resource_group.main.name}"
  loadbalancer_id     = "${azurerm_lb.intlb.id}"
  name                = "BackEndAddressPool"
}

resource "azurerm_dns_a_record" "intlb" {
  name                = "111-${var.locale-short}-${var.environment-short}-lb-int"
  zone_name           = "${var.locale-short}.111.service.nhs.uk"
  resource_group_name = "111-dns"
  ttl                 = 180
  records             = ["${azurerm_lb.intlb.private_ip_address}"]
}
#HAProxy LB Config
resource "azurerm_lb_probe" "HAIntProbe8181" {
  resource_group_name = "${azurerm_resource_group.main.name}"
  loadbalancer_id     = "${azurerm_lb.intlb.id}"
  name                = "probe-haproxy"
  port                = "8181"
  protocol            = "http"
  request_path        = "/"
  interval_in_seconds = "5"
}
resource "azurerm_lb_rule" "HAIntPort80" {
  resource_group_name            = "${azurerm_resource_group.main.name}"
  loadbalancer_id                = "${azurerm_lb.intlb.id}"
  name                           = "LBRule-80"
  protocol                       = "Tcp"
  frontend_port                  = "80"
  frontend_ip_configuration_name = "111-${var.locale-short}-${var.environment-short}-lb-int-ip"
  backend_port                   = "80"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.intlb.id}"
  probe_id                       = "${azurerm_lb_probe.HAIntProbe8181.id}"
}
resource "azurerm_lb_rule" "HAIntPort443" {
  resource_group_name            = "${azurerm_resource_group.main.name}"
  loadbalancer_id                = "${azurerm_lb.intlb.id}"
  name                           = "LBRule-443"
  protocol                       = "Tcp"
  frontend_port                  = "443"
  frontend_ip_configuration_name = "111-${var.locale-short}-${var.environment-short}-lb-int-ip"
  backend_port                   = "443"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.intlb.id}"
  probe_id                       = "${azurerm_lb_probe.HAIntProbe8181.id}"
}
