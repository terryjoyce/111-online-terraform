# Linux Docker Host One
module "windows_docker_host_1" {
  source            = "modules/windows_docker_host"
  host-name         = "wdk01"
  locale            = "${var.locale}"
  locale-short      = "${var.locale-short}"
  environment       = "${var.environment}"
  environment-short = "${var.environment-short}"
  resource-group    = "${azurerm_resource_group.main.name}"
  subnet            = "${azurerm_subnet.wds.id}"
  availability-set  = "${azurerm_availability_set.wds.id}"
  user              = "${var.global-user}"
  password          = "${var.global-password}"
}
module "windows_docker_host_2" {
  source            = "modules/windows_docker_host"
  host-name         = "wdk02"
  locale            = "${var.locale}"
  locale-short      = "${var.locale-short}"
  environment       = "${var.environment}"
  environment-short = "${var.environment-short}"
  resource-group    = "${azurerm_resource_group.main.name}"
  subnet            = "${azurerm_subnet.wds.id}"
  availability-set  = "${azurerm_availability_set.wds.id}"
  user              = "${var.global-user}"
  password          = "${var.global-password}"
}
