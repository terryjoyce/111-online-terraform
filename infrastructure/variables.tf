variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}


variable "environment" {}
variable "environment-short" {}
variable "locale" {}
variable "locale-short" {}
variable "address-middle" {}
variable "edge-address-end" {}
variable "lds-address-end" {}
variable "wds-address-end" {}
variable "bas-address-end" {}
variable "global-user" {}
variable "global-password" {}
variable "ssh-key" {}

variable "bas-port-102" { default = [""] }
variable "bas-source-102" {}
variable "bas-port-103" { default = [""] }
variable "bas-source-103" {}
variable "bas-port-104" {}
variable "bas-source-104" {}

variable "edge-port-102" {}
variable "edge-port-103" {}
variable "edge-source-103" { default = [""] }
variable "edge-port-104" {}
variable "edge-source-104" {}
variable "edge-port-105" {}
variable "edge-source-105" {}
variable "edge-port-106" { default = [""] }
variable "edge-source-106" {}
variable "edge-port-107" {}
variable "edge-source-107" { default = [""] }
variable "edge-port-108" {}
variable "edge-source-108" { default = [""] }
variable "edge-port-109" {}
variable "edge-port-110" {}
variable "edge-source-110" {}
variable "edge-port-111" {}
variable "edge-source-111" {}
variable "edge-port-112" {}
variable "edge-source-112" {}

variable "lds-port-102" {}
variable "lds-port-103" { default = [""] }
variable "lds-port-104" { default = [""] }
variable "lds-source-104" {}

variable "wds-port-102" {}
variable "wds-port-103" { default = [""] }
variable "wds-port-104" { default = [""] }
variable "wds-source-104" {}
variable "wds-port-105" {}
variable "wds-source-105" {}
