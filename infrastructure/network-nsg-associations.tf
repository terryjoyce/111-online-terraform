resource "azurerm_subnet_network_security_group_association" "edge" {
  subnet_id                 = "${azurerm_subnet.edge.id}"
  network_security_group_id = "${azurerm_network_security_group.nsg-edge.id}"
}

resource "azurerm_subnet_network_security_group_association" "lds" {
  subnet_id                 = "${azurerm_subnet.lds.id}"
  network_security_group_id = "${azurerm_network_security_group.nsg-lds.id}"
}

resource "azurerm_subnet_network_security_group_association" "wds" {
  subnet_id                 = "${azurerm_subnet.wds.id}"
  network_security_group_id = "${azurerm_network_security_group.nsg-wds.id}"
}

resource "azurerm_subnet_network_security_group_association" "bas" {
  subnet_id                 = "${azurerm_subnet.bas.id}"
  network_security_group_id = "${azurerm_network_security_group.nsg-bas.id}"
}
