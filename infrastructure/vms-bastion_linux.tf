# Linux Docker Host One
module "linux_bastion_host_1" {
  source            = "modules/linux_bastion_host"
  host-name         = "lbas01"
  locale            = "${var.locale}"
  locale-short      = "${var.locale-short}"
  environment       = "${var.environment}"
  environment-short = "${var.environment-short}"
  resource-group    = "${azurerm_resource_group.main.name}"
  subnet            = "${azurerm_subnet.bas.id}"
  user              = "${var.global-user}"
  ssh-key-full      = [{ path = "/home/${var.global-user}/.ssh/authorized_keys" key_data = "${var.ssh-key}"  }]
}
