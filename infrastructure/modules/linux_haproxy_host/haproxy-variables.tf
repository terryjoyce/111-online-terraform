variable "host-name" {}
variable "locale" {}
variable "locale-short" {}
variable "environment" {}
variable "environment-short" {}
variable "resource-group" {}
variable "user" {}
variable "ssh-key-full"  { default = [""] }
variable "subnet" {}
variable "availability-set" {}
