data "azurerm_public_ip" "wbas" {
    name = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-ip"
    resource_group_name = "${var.resource-group}"
    depends_on = ["azurerm_public_ip.wbas"]
}

resource "azurerm_public_ip" "wbas" {
  name                         = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-ip"
  location                     = "${var.locale}"
  resource_group_name          = "${var.resource-group}"
  public_ip_address_allocation = "static"
}

resource "azurerm_network_interface" "wbas" {
  name                            = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-nic"
  location                        = "${var.locale}"
  resource_group_name             = "${var.resource-group}"

  ip_configuration {
    name                          = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-nic-ip"
    subnet_id                     = "${var.subnet}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${data.azurerm_public_ip.wbas.id}"
  }
}

resource "azurerm_virtual_machine" "wbas" {
  name                  = "111-${var.locale-short}-${var.environment-short}-${var.host-name}"
  location              = "${var.locale}"
  resource_group_name   = "${var.resource-group}"
  network_interface_ids = ["${azurerm_network_interface.wbas.id}"]
  vm_size               = "Standard_A1_v2"

#DO NOT CHANGE THIS ONCE AN ENVIRONMENT HAS BEEN CREATED. ALL MACHINES WILL BE REBASED WITH THIS IMAGE!!!
storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter-with-Containers"
    version   = "latest"
  }

  storage_os_disk {
    name              = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "111-${var.locale-short}-${var.environment-short}-${var.host-name}"
    admin_username = "${var.user}"
    admin_password = "${var.password}"
  }
  os_profile_windows_config{
    enable_automatic_upgrades = "true"
    provision_vm_agent = "true"
  }
}

resource "azurerm_dns_a_record" "main" {
  name                = "111-${var.locale-short}-${var.environment-short}-${var.host-name}"
  zone_name           = "${var.locale-short}.111.service.nhs.uk"
  resource_group_name = "111-dns"
  ttl                 = 180
  records             = ["${data.azurerm_public_ip.wbas.ip_address}"]
}
