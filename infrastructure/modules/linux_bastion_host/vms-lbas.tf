data "azurerm_public_ip" "lbas" {
    name = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-ip"
    resource_group_name = "${var.resource-group}"
    depends_on = ["azurerm_public_ip.lbas"]
}

resource "azurerm_public_ip" "lbas" {
  name                         = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-ip"
  location                     = "${var.locale}"
  resource_group_name          = "${var.resource-group}"
  public_ip_address_allocation = "static"
}

resource "azurerm_network_interface" "lbas" {
  name                            = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-nic"
  location                        = "${var.locale}"
  resource_group_name             = "${var.resource-group}"

  ip_configuration {
    name                          = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-nic-ip"
    subnet_id                     = "${var.subnet}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${data.azurerm_public_ip.lbas.id}"
  }
}

resource "azurerm_virtual_machine" "lbas" {
  name                  = "111-${var.locale-short}-${var.environment-short}-${var.host-name}"
  location              = "${var.locale}"
  resource_group_name   = "${var.resource-group}"
  network_interface_ids = ["${azurerm_network_interface.lbas.id}"]
  vm_size               = "Standard_A1_v2"

storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.3"
    version   = "latest"
  }

  storage_os_disk {
    name              = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "111-${var.locale-short}-${var.environment-short}-${var.host-name}"
    admin_username = "${var.user}"
    admin_password = ""
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys = "${var.ssh-key-full}"
  }

}

resource "azurerm_dns_a_record" "main" {
  name                = "111-${var.locale-short}-${var.environment-short}-${var.host-name}"
  zone_name           = "${var.locale-short}.111.service.nhs.uk"
  resource_group_name = "111-dns"
  ttl                 = 180
  records             = ["${data.azurerm_public_ip.lbas.ip_address}"]
}
