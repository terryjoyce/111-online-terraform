data "azurerm_public_ip" "ldkm" {
    name = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-ip"
    resource_group_name = "${var.resource-group}"
    depends_on = ["azurerm_public_ip.ldkm"]
}

resource "azurerm_public_ip" "ldkm" {
  name                         = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-ip"
  location                     = "${var.locale}"
  resource_group_name          = "${var.resource-group}"
  public_ip_address_allocation = "static"
}

resource "azurerm_network_interface" "ldkm" {
  name                            = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-nic"
  location                        = "${var.locale}"
  resource_group_name             = "${var.resource-group}"

  ip_configuration {
    name                          = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-nic-ip"
    subnet_id                     = "${var.subnet}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${data.azurerm_public_ip.ldkm.id}"
  }
}

resource "azurerm_virtual_machine" "ldkm" {
  name                  = "111-${var.locale-short}-${var.environment-short}-${var.host-name}"
  location              = "${var.locale}"
  resource_group_name   = "${var.resource-group}"
  network_interface_ids = ["${azurerm_network_interface.ldkm.id}"]
  vm_size               = "Standard_DS11_v2"
  availability_set_id   = "${var.availability-set}"

#DO NOT CHANGE THIS ONCE AN ENVIRONMENT HAS BEEN CREATED. ALL MACHINES WILL BE REBASED WITH THIS IMAGE!!!
storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.3"
    version   = "latest"
  }

  storage_os_disk {
    name              = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "111-${var.locale-short}-${var.environment-short}-${var.host-name}"
    admin_username = "${var.user}"
    admin_password = ""
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys = "${var.ssh-key-full}"
  }

}

resource "azurerm_dns_a_record" "int" {
  name                = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-int"
  zone_name           = "${var.locale-short}.111.service.nhs.uk"
  resource_group_name = "111-dns"
  ttl                 = 180
  records             = ["${azurerm_network_interface.ldkm.private_ip_address}"]
}

resource "azurerm_dns_a_record" "ext" {
  name                = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-ext"
  zone_name           = "${var.locale-short}.111.service.nhs.uk"
  resource_group_name = "111-dns"
  ttl                 = 180
  records             = ["${azurerm_public_ip.ldkm.ip_address}"]
}
