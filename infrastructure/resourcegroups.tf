# Create a resource group
resource "azurerm_resource_group" "main" {
  name     = "111-${var.locale-short}-${var.environment}"
  location = "${var.locale}"
}
