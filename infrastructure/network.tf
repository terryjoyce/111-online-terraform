# Create a virtual network in the web_servers resource group
resource "azurerm_virtual_network" "main" {
  name                = "111-${var.locale-short}-vnet"
  address_space       = ["172.${var.address-middle}.0/24"]
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"
}

resource "azurerm_subnet" "edge" {
  name                      = "111-${var.locale-short}-edge-${var.environment}"
  resource_group_name       = "${azurerm_resource_group.main.name}"
  virtual_network_name      = "${azurerm_virtual_network.main.name}"
  address_prefix            = "172.${var.address-middle}.${var.edge-address-end}"
  network_security_group_id = "${azurerm_network_security_group.nsg-edge.id}"
}

resource "azurerm_subnet" "lds" {
  name                      = "111-${var.locale-short}-lds-${var.environment}"
  resource_group_name       = "${azurerm_resource_group.main.name}"
  virtual_network_name      = "${azurerm_virtual_network.main.name}"
  address_prefix            = "172.${var.address-middle}.${var.lds-address-end}"
  network_security_group_id = "${azurerm_network_security_group.nsg-lds.id}"
}

resource "azurerm_subnet" "wds" {
  name                      = "111-${var.locale-short}-wds-${var.environment}"
  resource_group_name       = "${azurerm_resource_group.main.name}"
  virtual_network_name      = "${azurerm_virtual_network.main.name}"
  address_prefix            = "172.${var.address-middle}.${var.wds-address-end}"
  network_security_group_id = "${azurerm_network_security_group.nsg-wds.id}"
}

resource "azurerm_subnet" "bas" {
  name                      = "111-${var.locale-short}-bas-${var.environment}"
  resource_group_name       = "${azurerm_resource_group.main.name}"
  virtual_network_name      = "${azurerm_virtual_network.main.name}"
  address_prefix            = "172.${var.address-middle}.${var.bas-address-end}"
  network_security_group_id = "${azurerm_network_security_group.nsg-bas.id}"
}
