resource "azurerm_network_security_group" "nsg-bas" {
  name                = "nsg-${var.locale-short}-bas-${var.environment}"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  security_rule {
    name                       = "LOOPBACK"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "172.${var.address-middle}.${var.bas-address-end}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "TJ_IN"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["${var.bas-port-102}"]
    source_address_prefix      = "${var.bas-source-102}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "NHSD_IN"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["${var.bas-port-103}"]
    source_address_prefix      = "${var.bas-source-103}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "BA01_IN"
    priority                   = 104
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.bas-port-104}"
    source_address_prefix      = "${var.bas-source-104}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "DENY_ALL"
    priority                   = 4096
    direction                  = "Inbound"
    access                     = "Deny"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
