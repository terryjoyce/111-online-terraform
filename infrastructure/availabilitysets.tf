resource "azurerm_availability_set" "lds" {
  name                = "ha-111-${var.locale-short}-${var.environment-short}-lds"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"
  managed             = "true"
  platform_fault_domain_count = "1"
}

resource "azurerm_availability_set" "wds" {
  name                = "ha-111-${var.locale-short}-${var.environment-short}-wds"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"
  managed             = "true"
  platform_fault_domain_count = "1"
}

resource "azurerm_availability_set" "haproxy" {
  name                = "ha-111-${var.locale-short}-${var.environment-short}-haproxy"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"
  managed             = "true"
  platform_fault_domain_count = "1"
}
