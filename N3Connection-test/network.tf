resource "azurerm_virtual_network" "main" {
  name                = "111-${var.locale-short}-sn-vnet"
  address_space       = ["${var.networksubnet}"]
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_subnet" "main" {
  name                      = "111-${var.locale-short}-sn-${var.environment}"
  resource_group_name       = "${azurerm_resource_group.main.name}"
  virtual_network_name      = "${azurerm_virtual_network.main.name}"
  address_prefix            = "${var.networksubnet}"
  network_security_group_id = "${azurerm_network_security_group.nsg-sn.id}"
}
