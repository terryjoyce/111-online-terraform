resource "azurerm_availability_set" "main" {
  name                = "111-${var.locale-short}-${var.environment-short}-mule-ha"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"
  managed             = "true"
  platform_update_domain_count = "1"
  platform_fault_domain_count = "1"
  
  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}
