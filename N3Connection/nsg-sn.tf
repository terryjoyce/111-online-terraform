resource "azurerm_network_security_group" "nsg-sn" {
  name                = "111-${var.locale-short}-sn-nsg"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  security_rule {
    name                       = "LOOPBACK"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "${var.networksubnet}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "BuildAgent_IN"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.port-102}"
    source_address_prefix      = "${var.source-102}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "BWP_IN"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.port-103}"
    source_address_prefix      = "${var.source-103}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "TJ_IN"
    priority                   = 104
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.port-104}"
    source_address_prefix      = "${var.source-104}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_ITKTEST_1"
    priority                   = 150
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "${var.itktest-uks-1}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_ITKTEST_2"
    priority                   = 151
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "${var.itktest-uks-2}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_ITKTEST_3"
    priority                   = 152
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "${var.itktest-uks-3}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_ITKTEST_4"
    priority                   = 153
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "${var.itktest-uks-4}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_ITKTEST_5"
    priority                   = 154
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "${var.itktest-uks-5}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_INT4"
    priority                   = 155
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefixes    = "${var.int4}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_INT1"
    priority                   = 160
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "51.140.224.12/32"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "TM_IN_HTTPS"
    priority                   = 200
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "AzureTrafficManager"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_01"
    priority                   = 201
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "104.112.3.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_02"
    priority                   = 202
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "104.116.163.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_03"
    priority                   = 203
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "104.118.105.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_04"
    priority                   = 204
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "106.187.61.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_05"
    priority                   = 205
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "117.104.138.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_06"
    priority                   = 206
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "117.239.240.0/25"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_07"
    priority                   = 207
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "165.254.1.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_08"
    priority                   = 208
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "202.239.172.64/26"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_09"
    priority                   = 209
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "204.156.7.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_10"
    priority                   = 210
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "207.86.215.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_11"
    priority                   = 211
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "209.116.151.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_12"
    priority                   = 212
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "23.205.116.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_13"
    priority                   = 213
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "23.219.39.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_14"
    priority                   = 214
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "23.4.240.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_15"
    priority                   = 215
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "59.144.112.128/25"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_16"
    priority                   = 216
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "60.254.143.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_17"
    priority                   = 217
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "61.9.129.128/25"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_18"
    priority                   = 218
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "63.217.232.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_19"
    priority                   = 219
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "63.233.60.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_20"
    priority                   = 220
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "63.233.61.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_21"
    priority                   = 221
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "72.246.191.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_22"
    priority                   = 222
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "80.156.248.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_23"
    priority                   = 223
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "95.100.169.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_24"
    priority                   = 224
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "2.19.97.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_25"
    priority                   = 225
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "2.21.243.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_26"
    priority                   = 226
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "2.22.242.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_27"
    priority                   = 227
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "72.246.231.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_28"
    priority                   = 228
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "84.53.138.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_29"
    priority                   = 229
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "88.221.222.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_30"
    priority                   = 230
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "88.221.89.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_31"
    priority                   = 231
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "92.122.54.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_32"
    priority                   = 232
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "92.123.142.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_33"
    priority                   = 233
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "92.123.66.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_34"
    priority                   = 234
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "95.101.143.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_35"
    priority                   = 235
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "95.101.39.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_36"
    priority                   = 236
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "96.17.152.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "MONITORING_IN"
    priority                   = 301
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.port-301}"
    source_address_prefix      = "${var.source-301}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "MONITORING_IN_2"
    priority                   = 302
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.port-302}"
    source_address_prefix      = "${var.source-302}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "DENY_ALL"
    priority                   = 4096
    direction                  = "Inbound"
    access                     = "Deny"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}
