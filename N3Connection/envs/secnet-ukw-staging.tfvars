#Environment and Location Variables
environment = "secnet-staging"
environment-short = "sn"
environment-ps = "s"

locale = "UK West"
locale-short = "ukw"
