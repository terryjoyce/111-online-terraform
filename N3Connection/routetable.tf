resource "azurerm_route_table" "test" {
  name                = "111-${var.locale-short}-sn-routetable"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  route {
    name           = "Internet"
    address_prefix = "0.0.0.0/0"
    next_hop_type  = "Internet"
  }
  route {
    name           = "RouteDNS1"
    address_prefix = "${var.dns1}/32"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
  route {
    name           = "RouteDNS2"
    address_prefix = "${var.dns2}/32"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
  route {
    name           = "SecureRange1"
    address_prefix = "${var.secnet-range-1}"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
  route {
    name           = "SecureRange2"
    address_prefix = "${var.secnet-range-2}"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
  route {
    name           = "SecureRange3"
    address_prefix = "${var.secnet-range-3}"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
  route {
    name           = "SecureRange4"
    address_prefix = "${var.secnet-range-4}"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
  route {
    name           = "SecureRange5"
    address_prefix = "${var.secnet-range-5}"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}
