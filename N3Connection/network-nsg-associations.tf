resource "azurerm_subnet_network_security_group_association" "nsg-sn" {
  subnet_id                 = "${azurerm_subnet.main.id}"
  network_security_group_id = "${azurerm_network_security_group.nsg-sn.id}"
}
