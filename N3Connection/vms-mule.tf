# Linux Docker Host One
module "linux_host_1" {
  source            = "./modules/linux_host"
  host-name         = "mule01"
  locale            = "${var.locale}"
  locale-short      = "${var.locale-short}"
  environment       = "${var.environment}"
  environment-short = "${var.environment-short}"
  environment-ps    = "${var.environment-ps}"
  resource-group    = "${azurerm_resource_group.main.name}"
  subnet            = "${azurerm_subnet.main.id}"
  availability-set  = "${azurerm_availability_set.main.id}"
  user              = "${var.global-user}"
  ssh-key           = "${var.ssh-key}"
}
