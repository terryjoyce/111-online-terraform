resource "azurerm_route_table" "test" {
  name                = "111-${var.locale-short}-sn-routetable"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  route {
    name           = "Internet"
    address_prefix = "0.0.0.0/0"
    next_hop_type  = "Internet"
  }
  route {
    name           = "RouteDNS1"
    address_prefix = "194.72.7.137/32"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
  route {
    name           = "RouteDNS2"
    address_prefix = "194.72.7.142/32"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
  route {
    name           = "SecureRange1"
    address_prefix = "10.0.0.0/8"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
  route {
    name           = "SecureRange2"
    address_prefix = "172.16.0.0/12"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
  route {
    name           = "SecureRange3"
    address_prefix = "20.146.0.0/16"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
  route {
    name           = "SecureRange4"
    address_prefix = "155.231.0.0/16"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
  route {
    name           = "SecureRange5"
    address_prefix = "212.196.44.0/24"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = "${var.nexthop}"
  }
}
