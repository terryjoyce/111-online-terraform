# Linux Docker Host One
module "linux_host_1" {
  source            = "modules/linux_host"
  host-name         = "mule01"
  locale            = "${var.locale}"
  locale-short      = "${var.locale-short}"
  environment       = "${var.environment}"
  environment-short = "${var.environment-short}"
  environment-ps    = "${var.environment-ps}"
  resource-group    = "${azurerm_resource_group.main.name}"
  subnet            = "${azurerm_subnet.main.id}"
  user              = "${var.global-user}"
  ssh-key-full      = [{ path = "/home/${var.global-user}/.ssh/authorized_keys" key_data = "${var.ssh-key}"  }]
}
