resource "azurerm_network_security_group" "nsg-sn" {
  name                = "111-${var.locale-short}-sn-nsg"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  security_rule {
    name                       = "LOOPBACK"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "172.28.194.0/23"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "BuildAgent_IN_SSH"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "51.140.71.68/32"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "BWP_IN_SSH"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "194.189.27.178/32"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "TJ_IN_SSH"
    priority                   = 104
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "92.239.156.184/32"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "TM_IN_HTTPS"
    priority                   = 200
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "AzureTrafficManager"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_01"
    priority                   = 201
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "104.112.3.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_02"
    priority                   = 202
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "104.116.163.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_04"
    priority                   = 203
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "117.104.138.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_05"
    priority                   = 204
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "117.239.240.0/25"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_06"
    priority                   = 205
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "165.254.1.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_07"
    priority                   = 206
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "204.156.7.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_08"
    priority                   = 207
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "204.188.136.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_09"
    priority                   = 208
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "207.86.215.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_10"
    priority                   = 209
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "209.116.151.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_11"
    priority                   = 210
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "23.205.116.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_12"
    priority                   = 211
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "23.219.39.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_13"
    priority                   = 212
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "23.4.240.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_14"
    priority                   = 213
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "59.144.112.128/25"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_15"
    priority                   = 214
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "60.254.143.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_16"
    priority                   = 215
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "61.9.129.128/25"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_17"
    priority                   = 216
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "63.217.232.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_18"
    priority                   = 217
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "63.233.60.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_19"
    priority                   = 218
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "63.233.61.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_AKAMAI_20"
    priority                   = 219
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "95.100.169.0/24"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "MONITORING_IN_NRPE"
    priority                   = 301
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5666"
    source_address_prefix      = "51.140.37.126"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "MONITORING_IN_HTTPS"
    priority                   = 302
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "51.140.37.126"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "DENY_ALL"
    priority                   = 4096
    direction                  = "Inbound"
    access                     = "Deny"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
