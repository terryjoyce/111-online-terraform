#Environment and Location Variables
environment = "secnet-staging"
environment-short = "sn"
environment-ps = "s"

locale = "UK South"
locale-short = "uks"

#Password Variables
ssh-key = ""
global-user = ""

#Network Variables
networksubnet = "172.28.194.32/28"
dns1 = "194.72.7.137"
dns2 = "194.72.7.142"
nexthop = "172.17.169.5"
