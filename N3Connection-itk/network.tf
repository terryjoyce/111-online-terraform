resource "azurerm_virtual_network" "main" {
  name                = "111-${var.locale-short}-sn-vnet"
  address_space       = ["${var.networksubnet}"]
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"
  dns_servers         = ["${var.dns1}", "${var.dns2}"]
}

resource "azurerm_subnet" "main" {
  name                      = "111-${var.locale-short}-sn-${var.environment}"
  resource_group_name       = "${azurerm_resource_group.main.name}"
  virtual_network_name      = "${azurerm_virtual_network.main.name}"
  address_prefix            = "${var.networksubnet}"
  network_security_group_id = "${azurerm_network_security_group.nsg-sn.id}"
  route_table_id            = "${azurerm_route_table.test.id}"
}
