# 111 Online Terraform

This is provided for information / copying purposes only.  It does not contain any credentials or keys required to work.

**Web Apps Infrastructure** is the config that is used for our the production and staging environments

**Infrastructure** is the config that was used for our the old production and staging environments

**Integration** is that environment

**Common** is setup for generic firewall changes with common assets

**N3 Connection** is our setup to provide a base infrastructure before we connect to the Health Network

**N3 Connection Test** is not connected to N3 and never will be and only contains test endpoints that mimic services on the health network that we call that do not have an Internet option
