# Linux Docker Host One
module "linux_docker_host_1" {
  source            = "./modules/linux_splunk_host"
  host-name         = "splunk01"
  locale            = "${var.locale}"
  locale-short      = "${var.locale-short}"
  environment       = "${var.environment}"
  environment-short = "${var.environment-short}"
  resource-group    = "${azurerm_resource_group.main.name}"
  subnet            = "${azurerm_subnet.splunk.id}"
  vm-size           = "${var.vm-size}"
  user              = "${var.global-user}"
  ssh-key           = "${var.ssh-key}"
}
