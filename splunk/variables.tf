variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "environment" {}
variable "environment-short" {}
variable "locale" {}
variable "locale-short" {}
variable "address-middle" {}
variable "splunk-address-end" {}
variable "vm-size" {}
variable "global-user" {}
variable "ssh-key" {}
variable "domain" {}
variable "port-102" {}
variable "source-102" {}
variable "port-103" {}
variable "source-103" {}
