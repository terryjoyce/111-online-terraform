# Create a virtual network in the web_servers resource group
resource "azurerm_virtual_network" "main" {
  name                = "111-${var.locale-short}-splunk-vnet"
  address_space       = ["172.${var.address-middle}.0/24"]
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_subnet" "splunk" {
  name                      = "111-${var.locale-short}-${var.environment}"
  resource_group_name       = "${azurerm_resource_group.main.name}"
  virtual_network_name      = "${azurerm_virtual_network.main.name}"
  address_prefix            = "172.${var.address-middle}.${var.splunk-address-end}"
  network_security_group_id = "${azurerm_network_security_group.nsg-splunk.id}"
}
