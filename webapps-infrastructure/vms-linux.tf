# Linux Docker Host One
module "linux_docker_host_1" {
  source            = "./modules/linux_docker_host"
  host-name         = "ldk01"
  locale            = "${var.locale}"
  locale-short      = "${var.locale-short}"
  environment       = "${var.environment}"
  environment-short = "${var.environment-short}"
  resource-group    = "${azurerm_resource_group.main.name}"
  subnet            = "${azurerm_subnet.lds.id}"
  availability-set  = "${azurerm_availability_set.lds.id}"
  user              = "${var.global-user}"
  ssh-key           = "${var.ssh-key}"
}
module "linux_docker_host_2" {
  source            = "./modules/linux_docker_host"
  host-name         = "ldk02"
  locale            = "${var.locale}"
  locale-short      = "${var.locale-short}"
  environment       = "${var.environment}"
  environment-short = "${var.environment-short}"
  resource-group    = "${azurerm_resource_group.main.name}"
  subnet            = "${azurerm_subnet.lds.id}"
  availability-set  = "${azurerm_availability_set.lds.id}"
  user              = "${var.global-user}"
  ssh-key           = "${var.ssh-key}"
}
