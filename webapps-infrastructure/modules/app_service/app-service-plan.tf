resource "azurerm_app_service_plan" "main" {
  name                = "111-${var.locale-short}-${var.environment}-asp"
  location            = "${var.locale}"
  resource_group_name = "${var.resource-group}"

  sku {
    tier = "${var.teir-windows}"
    size = "${var.size-windows}"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}
