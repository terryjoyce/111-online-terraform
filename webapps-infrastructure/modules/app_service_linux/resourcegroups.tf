# Create a resource group
resource "azurerm_resource_group" "linux" {
  name     = "111-${var.environment}-${var.locale-short}-linux"
  location = "${var.locale}"

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}
