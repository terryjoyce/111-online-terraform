resource "azurerm_app_service" "neo4j" {
  name                    = "111-${var.locale-short}-${var.environment-short}-neo4j"
  location                = "${var.locale}"
  resource_group_name     = "${azurerm_resource_group.linux.name}"
  app_service_plan_id     = "${azurerm_app_service_plan.main.id}"
  https_only              = "true"
  client_affinity_enabled = "false"

  site_config {
    always_on               = "true"
    min_tls_version         = "${var.tlsversion}"
    #linux_fx_version        = "DOCKER|${var.docker_url}/111-neo4j:latest"
    ftps_state              = "Disabled"

  ip_restriction {
    ip_address="${var.uks1-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.uks2-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.uks3-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.uks4-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.uks5-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.uks6-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.uks7-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.ukw1-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.ukw2-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.ukw3-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.ukw4-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.ukw5-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.ukw6-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.ukw7-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.ukw8-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.nagios-ip}"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="${var.octopus-ip}"
    subnet_mask="255.255.255.255"
  }
#Azure Traffic Manager IP Addresses
  ip_restriction {
    ip_address="40.68.30.66"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="40.68.31.178"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="137.135.80.149"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="137.135.82.249"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="23.96.236.252"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="65.52.217.19"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="40.87.147.10"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="40.87.151.34"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="13.75.124.254"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="13.75.127.63"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="52.172.155.168"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="52.172.158.37"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="104.215.91.84"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="13.75.153.124"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="13.84.222.37"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="23.101.191.199"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="23.96.213.12"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="137.135.46.163"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="137.135.47.215"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="191.232.208.52"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="191.232.214.62"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="13.75.152.253"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="104.41.187.209"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="104.41.190.203"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="52.173.90.107"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="52.173.250.232"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="104.45.149.110"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="40.114.5.197"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="52.240.151.125"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="52.240.144.45"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="13.65.95.152"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="13.65.92.252"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="40.78.67.110"
    subnet_mask="255.255.255.255"
  }
  ip_restriction {
    ip_address="104.42.192.195"
    subnet_mask="255.255.255.255"
  }
}

#  app_settings = {

#    "WEBSITES_ENABLE_APP_SERVICE_STORAGE" = "false"
#    "DOCKER_REGISTRY_SERVER_URL"          = "${var.docker_url}"
#  "DOCKER_REGISTRY_SERVER_USERNAME"     = "${var.docker_username}"
#    "DOCKER_REGISTRY_SERVER_PASSWORD"     = "${var.docker_password}"
#  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_app_service_custom_hostname_binding" "neo4j" {
  hostname            = "111-${var.environment-short}-neo4j.${var.domain}"
  app_service_name    = "${azurerm_app_service.neo4j.name}"
  resource_group_name = "${azurerm_resource_group.linux.name}"
}
