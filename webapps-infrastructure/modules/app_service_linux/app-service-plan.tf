resource "azurerm_app_service_plan" "main" {
  name                = "111-${var.locale-short}-${var.environment}-linux-asp"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.linux.name}"
  kind                = "linux"
  reserved            = true
  
  sku {
    tier = "${var.teir-linux}"
    size = "${var.size-linux}"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}
