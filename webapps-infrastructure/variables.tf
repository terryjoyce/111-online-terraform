variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}

variable "environment" {}
variable "environment-short" {}
variable "locale" {}
variable "locale-short" {}
variable "address-middle" {}
variable "edge-address-end" {}
variable "lds-address-end" {}
variable "bas-address-end" {}
variable "global-user" {}
variable "ssh-key" {}
variable "domain" {}
variable "docker_url" {}
variable "docker_username" {}
variable "docker_password" {}
variable "app-insights" {}

variable "size-windows" {}
variable "tier-windows" {}
variable "size-linux" {}
variable "tier-linux" {}

variable "uks1-ip" {}
variable "uks2-ip" {}
variable "uks3-ip" {}
variable "uks4-ip" {}
variable "uks5-ip" {}
variable "uks6-ip" {}
variable "uks7-ip" {}
variable "ukw1-ip" {}
variable "ukw2-ip" {}
variable "ukw3-ip" {}
variable "ukw4-ip" {}
variable "ukw5-ip" {}
variable "ukw6-ip" {}
variable "ukw7-ip" {}
variable "ukw8-ip" {}
variable "analytics1-ip" {}
variable "analytics2-ip" {}
variable "analytics3-ip" {}
variable "analytics4-ip" {}
variable "analytics5-ip" {}
variable "analytics6-ip" {}
variable "analytics7-ip" {}
variable "octopus-ip" {}
variable "nagios-ip" {}
variable "secnet-uks-ip" {}
variable "secnet-ukw-ip" {}
variable "haproxy-uks-ip1" {}
variable "haproxy-uks-ip2" {}
variable "haproxy-ukw-ip1" {}
variable "haproxy-ukw-ip2" {}

variable "bas-port-102" { default = [""] }
variable "bas-source-102" {}
variable "bas-port-103" { default = [""] }
variable "bas-source-103" {}
variable "bas-port-104" {}
variable "bas-source-104" {}

variable "edge-port-102" {}
variable "edge-port-103" {}
variable "edge-source-103" {}
variable "edge-port-104" {}
variable "edge-source-104" {}
variable "edge-port-105" { default = [""] }
variable "edge-source-105" {}
variable "edge-port-106" {}
variable "edge-source-106" {}
variable "edge-port-107" {}
variable "edge-source-107" {}
variable "edge-port-108" {}
variable "edge-source-108" {}

variable "lds-port-102" {}
variable "lds-port-103" { default = [""] }
variable "lds-port-104" { default = [""] }
variable "lds-source-104" {}
