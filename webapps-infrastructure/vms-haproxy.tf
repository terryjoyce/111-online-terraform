# Linux Docker Host One
module "linux_lb_host_1" {
  source            = "./modules/linux_haproxy_host"
  host-name         = "lb01"
  locale            = "${var.locale}"
  locale-short      = "${var.locale-short}"
  environment       = "${var.environment}"
  environment-short = "${var.environment-short}"
  resource-group    = "${azurerm_resource_group.main.name}"
  subnet            = "${azurerm_subnet.edge.id}"
  availability-set  = "${azurerm_availability_set.haproxy.id}"
  user              = "${var.global-user}"
  ssh-key           = "${var.ssh-key}"
}
module "linux_lb_host_2" {
  source            = "./modules/linux_haproxy_host"
  host-name         = "lb02"
  locale            = "${var.locale}"
  locale-short      = "${var.locale-short}"
  environment       = "${var.environment}"
  environment-short = "${var.environment-short}"
  resource-group    = "${azurerm_resource_group.main.name}"
  subnet            = "${azurerm_subnet.edge.id}"
  availability-set  = "${azurerm_availability_set.haproxy.id}"
  user              = "${var.global-user}"
  ssh-key           = "${var.ssh-key}"
}
