resource "azurerm_public_ip" "extlb" {
  name                         = "111-${var.locale-short}-${var.environment-short}-lb-ext-ip"
  location                     = "${var.locale}"
  resource_group_name          = "${azurerm_resource_group.main.name}"
  allocation_method            = "Static"
  domain_name_label            = "${var.locale-short}-${var.environment-short}-lb"

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_lb" "extlb" {
  name                = "111-${var.locale-short}-${var.environment-short}-lb-ext"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  frontend_ip_configuration {
    name                 = "111-${var.locale-short}-${var.environment-short}-lb-ext-ip"
    public_ip_address_id = "${azurerm_public_ip.extlb.id}"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_lb_backend_address_pool" "extlb" {
  resource_group_name = "${azurerm_resource_group.main.name}"
  loadbalancer_id     = "${azurerm_lb.extlb.id}"
  name                = "BackEndAddressPool"
}

resource "azurerm_dns_a_record" "extlb" {
  name                = "111-${var.locale-short}-${var.environment-short}-lb-ext"
  zone_name           = "${var.locale-short}.111.nhs.uk"
  resource_group_name = "111-dns"
  ttl                 = 180
  records             = ["${azurerm_public_ip.extlb.ip_address}"]
}
#HAProxy LB Config
resource "azurerm_lb_probe" "HAProbe8181" {
  resource_group_name = "${azurerm_resource_group.main.name}"
  loadbalancer_id     = "${azurerm_lb.extlb.id}"
  name                = "probe-haproxy"
  port                = "8181"
  protocol            = "http"
  request_path        = "/"
  interval_in_seconds = "5"
}
resource "azurerm_lb_rule" "HAPort80" {
  resource_group_name            = "${azurerm_resource_group.main.name}"
  loadbalancer_id                = "${azurerm_lb.extlb.id}"
  name                           = "LBRule-80"
  protocol                       = "Tcp"
  frontend_port                  = "80"
  frontend_ip_configuration_name = "111-${var.locale-short}-${var.environment-short}-lb-ext-ip"
  backend_port                   = "80"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.extlb.id}"
  probe_id                       = "${azurerm_lb_probe.HAProbe8181.id}"
}
resource "azurerm_lb_rule" "HAPort443" {
  resource_group_name            = "${azurerm_resource_group.main.name}"
  loadbalancer_id                = "${azurerm_lb.extlb.id}"
  name                           = "LBRule-443"
  protocol                       = "Tcp"
  frontend_port                  = "443"
  frontend_ip_configuration_name = "111-${var.locale-short}-${var.environment-short}-lb-ext-ip"
  backend_port                   = "443"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.extlb.id}"
  probe_id                       = "${azurerm_lb_probe.HAProbe8181.id}"
}
resource "azurerm_lb_rule" "HAPort8181" {
  resource_group_name            = "${azurerm_resource_group.main.name}"
  loadbalancer_id                = "${azurerm_lb.extlb.id}"
  name                           = "LBRule-8181"
  protocol                       = "Tcp"
  frontend_port                  = "8181"
  frontend_ip_configuration_name = "111-${var.locale-short}-${var.environment-short}-lb-ext-ip"
  backend_port                   = "8181"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.extlb.id}"
  probe_id                       = "${azurerm_lb_probe.HAProbe8181.id}"
}
