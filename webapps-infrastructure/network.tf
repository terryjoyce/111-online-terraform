# Create a virtual network in the web_servers resource group
resource "azurerm_virtual_network" "main" {
  name                = "111-${var.locale-short}-vnet"
  address_space       = ["172.${var.address-middle}.0/24"]
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_subnet" "edge" {
  name                      = "111-${var.locale-short}-edge-${var.environment}"
  resource_group_name       = "${azurerm_resource_group.main.name}"
  virtual_network_name      = "${azurerm_virtual_network.main.name}"
  address_prefix            = "172.${var.address-middle}.${var.edge-address-end}"
  network_security_group_id = "${azurerm_network_security_group.nsg-edge.id}"
}

resource "azurerm_subnet" "lds" {
  name                      = "111-${var.locale-short}-lds-${var.environment}"
  resource_group_name       = "${azurerm_resource_group.main.name}"
  virtual_network_name      = "${azurerm_virtual_network.main.name}"
  address_prefix            = "172.${var.address-middle}.${var.lds-address-end}"
  network_security_group_id = "${azurerm_network_security_group.nsg-lds.id}"
}

resource "azurerm_subnet" "bas" {
  name                      = "111-${var.locale-short}-bas-${var.environment}"
  resource_group_name       = "${azurerm_resource_group.main.name}"
  virtual_network_name      = "${azurerm_virtual_network.main.name}"
  address_prefix            = "172.${var.address-middle}.${var.bas-address-end}"
  network_security_group_id = "${azurerm_network_security_group.nsg-bas.id}"
}
