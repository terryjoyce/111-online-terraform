resource "azurerm_network_security_group" "nsg-lds" {
    name                = "nsg-${var.locale-short}-lds-${var.environment}"
    location            = "${var.locale}"
    resource_group_name = "${azurerm_resource_group.main.name}"

    security_rule {
      name                       = "LOOPBACK"
      priority                   = 101
      direction                  = "Inbound"
      access                     = "Allow"
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_range     = "*"
      source_address_prefix      = "172.${var.address-middle}.${var.lds-address-end}"
      destination_address_prefix = "*"
    }
    security_rule {
      name                       = "BAS_IN"
      priority                   = 102
      direction                  = "Inbound"
      access                     = "Allow"
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_range     = "${var.lds-port-102}"
      source_address_prefix      = "172.${var.address-middle}.${var.bas-address-end}"
      destination_address_prefix = "*"
    }
    security_rule {
      name                       = "EDGE_IN"
      priority                   = 103
      direction                  = "Inbound"
      access                     = "Allow"
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_ranges    = "${var.lds-port-103}"
      source_address_prefix      = "172.${var.address-middle}.${var.edge-address-end}"
      destination_address_prefix = "*"
    }
    security_rule {
      name                       = "MONITORING_IN"
      priority                   = 104
      direction                  = "Inbound"
      access                     = "Allow"
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_ranges    = "${var.lds-port-104}"
      source_address_prefix      = "${var.lds-source-104}"
      destination_address_prefix = "*"
    }
    security_rule {
      name                       = "DENY_ALL"
      priority                   = 4096
      direction                  = "Inbound"
      access                     = "Deny"
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_range     = "*"
      source_address_prefix      = "*"
      destination_address_prefix = "*"
    }
    tags = {
      TagVersion = "0.2",
      Program = "111 Online",
      Product = "111 Online",
      Owner = "Debbie Floyd",
      TechnicalContact = "Terry Joyce",
      CostCentre = "P0436/02",
      DataClassification = "5",
      Environment = "${var.environment}",
      EnvironmentLocation = "${var.locale}",
      ServiceCategory = "Silver"
    }
}
