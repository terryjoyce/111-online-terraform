variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "environment" {}
variable "environment-short" {}
variable "locale" {}
variable "locale-short" {}
variable "address-middle" {}
variable "integration-address-end" {}
variable "global-user" {}
variable "ssh-key" {}
variable "port-102" { default = [""] }
variable "source-102" {}
variable "port-103" { default = [""] }
variable "source-103" {}
variable "port-104" { default = [""] }
variable "source-104" {}
variable "port-105" { default = [""] }
variable "source-105" {}
variable "port-201" { default = [""] }
variable "source-201" {}
variable "port-301" { default = [""] }
variable "source-301" {}
variable "port-302" { default = [""] }
variable "source-302" {}
variable "port-303" { default = [""] }
variable "source-303" {}
variable "port-304" { default = [""] }
variable "source-304" {}
