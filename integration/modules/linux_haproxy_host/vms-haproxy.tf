data "azurerm_public_ip" "lbh" {
    name = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-ip"
    resource_group_name = "${var.resource-group}"
    depends_on = ["azurerm_public_ip.lbh"]
}

resource "azurerm_public_ip" "lbh" {
  name                         = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-ip"
  location                     = "${var.locale}"
  resource_group_name          = "${var.resource-group}"
  allocation_method            = "Static"

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_network_interface" "lbh" {
  name                            = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-nic"
  location                        = "${var.locale}"
  resource_group_name             = "${var.resource-group}"

  ip_configuration {
    name                          = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-nic-ip"
    subnet_id                     = "${var.subnet}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${data.azurerm_public_ip.lbh.id}"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_virtual_machine" "lbh" {
  name                  = "111-${var.locale-short}-${var.environment-short}-${var.host-name}"
  location              = "${var.locale}"
  resource_group_name   = "${var.resource-group}"
  network_interface_ids = ["${azurerm_network_interface.lbh.id}"]
  vm_size               = "Standard_A1"

#DO NOT CHANGE THIS ONCE AN ENVIRONMENT HAS BEEN CREATED. ALL MACHINES WILL BE REBASED WITH THIS IMAGE!!!
storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.4"
    version   = "latest"
  }

  storage_os_disk {
    name              = "111-${var.locale-short}-${var.environment-short}-${var.host-name}-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "111-${var.locale-short}-${var.environment-short}-${var.host-name}"
    admin_username = "${var.user}"
    admin_password = ""
  }

  os_profile_linux_config {
      disable_password_authentication = true
      ssh_keys {
          path     = "/home/${var.user}/.ssh/authorized_keys"
          key_data = "${var.ssh-key}"
      }
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_dns_a_record" "main" {
  name                = "111-${var.locale-short}-${var.environment-short}-${var.host-name}"
  zone_name           = "${var.locale-short}.111.service.nhs.uk"
  resource_group_name = "111-dns"
  ttl                 = 180
  records             = ["${azurerm_public_ip.lbh.ip_address}"]

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}
