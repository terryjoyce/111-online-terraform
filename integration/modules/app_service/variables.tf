variable "intnumber" {}
variable "tlsversion" {}
variable "size" {}
variable "locale" {}
variable "locale-short" {}
variable "environment" {}
variable "environment-short" {}
variable "resource-group" {}
