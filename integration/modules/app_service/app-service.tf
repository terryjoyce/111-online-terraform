resource "azurerm_app_service" "web" {
  name                    = "111-${var.locale-short}-${var.environment-short}-web-${var.intnumber}"
  location                = "${var.locale}"
  resource_group_name     = "${var.resource-group}"
  app_service_plan_id     = "${azurerm_app_service_plan.main.id}"
  https_only              = "true"
  client_affinity_enabled = "false"

  site_config {
    always_on               = "true"
    min_tls_version         = "${var.tlsversion}"
  }

  app_settings = {
    APPINSIGHTS_INSTRUMENTATIONKEY = "${azurerm_application_insights.main.instrumentation_key}"
    APPINSIGHTS_PROFILERFEATURE_VERSION = "1.0.0"
    APPINSIGHTS_SNAPSHOTFEATURE_VERSION = "1.0.0"
    ApplicationInsightsAgent_EXTENSION_VERSION = "~2"
    DiagnosticServices_EXTENSION_VERSION = "~3"
    InstrumentationEngine_EXTENSION_VERSION = "~1"
    SnapshotDebugger_EXTENSION_VERSION = "~1"
    XDT_MicrosoftApplicationInsights_BaseExtensions = "~1"
    WEBSITE_TIME_ZONE = "GMT Standard Time"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_app_service" "auditapi" {
  name                    = "111-${var.locale-short}-${var.environment-short}-audit-api-${var.intnumber}"
  location                = "${var.locale}"
  resource_group_name     = "${var.resource-group}"
  app_service_plan_id     = "${azurerm_app_service_plan.main.id}"
  https_only              = "true"
  client_affinity_enabled = "false"

  site_config {
    always_on               = "true"
    min_tls_version         = "${var.tlsversion}"
  }

  app_settings = {
    APPINSIGHTS_INSTRUMENTATIONKEY = "${azurerm_application_insights.main.instrumentation_key}"
    APPINSIGHTS_PROFILERFEATURE_VERSION = "1.0.0"
    APPINSIGHTS_SNAPSHOTFEATURE_VERSION = "1.0.0"
    ApplicationInsightsAgent_EXTENSION_VERSION = "~2"
    DiagnosticServices_EXTENSION_VERSION = "~3"
    InstrumentationEngine_EXTENSION_VERSION = "~1"
    SnapshotDebugger_EXTENSION_VERSION = "~1"
    XDT_MicrosoftApplicationInsights_BaseExtensions = "~1"
    WEBSITE_TIME_ZONE = "GMT Standard Time"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_app_service" "businessapi" {
  name                    = "111-${var.locale-short}-${var.environment-short}-business-api-${var.intnumber}"
  location                = "${var.locale}"
  resource_group_name     = "${var.resource-group}"
  app_service_plan_id     = "${azurerm_app_service_plan.main.id}"
  https_only              = "true"
  client_affinity_enabled = "false"

  site_config {
    always_on               = "true"
    min_tls_version         = "${var.tlsversion}"
  }

  app_settings = {
    APPINSIGHTS_INSTRUMENTATIONKEY = "${azurerm_application_insights.main.instrumentation_key}"
    APPINSIGHTS_PROFILERFEATURE_VERSION = "1.0.0"
    APPINSIGHTS_SNAPSHOTFEATURE_VERSION = "1.0.0"
    ApplicationInsightsAgent_EXTENSION_VERSION = "~2"
    DiagnosticServices_EXTENSION_VERSION = "~3"
    InstrumentationEngine_EXTENSION_VERSION = "~1"
    SnapshotDebugger_EXTENSION_VERSION = "~1"
    XDT_MicrosoftApplicationInsights_BaseExtensions = "~1"
    WEBSITE_TIME_ZONE = "GMT Standard Time"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_app_service" "businessdosapi" {
  name                    = "111-${var.locale-short}-${var.environment-short}-business-dos-api-${var.intnumber}"
  location                = "${var.locale}"
  resource_group_name     = "${var.resource-group}"
  app_service_plan_id     = "${azurerm_app_service_plan.main.id}"
  https_only              = "true"
  client_affinity_enabled = "false"

  site_config {
    always_on               = "true"
    min_tls_version         = "${var.tlsversion}"
  }

  app_settings = {
    APPINSIGHTS_INSTRUMENTATIONKEY = "${azurerm_application_insights.main.instrumentation_key}"
    APPINSIGHTS_PROFILERFEATURE_VERSION = "1.0.0"
    APPINSIGHTS_SNAPSHOTFEATURE_VERSION = "1.0.0"
    ApplicationInsightsAgent_EXTENSION_VERSION = "~2"
    DiagnosticServices_EXTENSION_VERSION = "~3"
    InstrumentationEngine_EXTENSION_VERSION = "~1"
    SnapshotDebugger_EXTENSION_VERSION = "~1"
    XDT_MicrosoftApplicationInsights_BaseExtensions = "~1"
    WEBSITE_TIME_ZONE = "GMT Standard Time"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_app_service" "ccgapi" {
  name                    = "111-${var.locale-short}-${var.environment-short}-ccg-api-${var.intnumber}"
  location                = "${var.locale}"
  resource_group_name     = "${var.resource-group}"
  app_service_plan_id     = "${azurerm_app_service_plan.main.id}"
  https_only              = "true"
  client_affinity_enabled = "false"

  site_config {
    always_on               = "true"
    min_tls_version         = "${var.tlsversion}"
  }

  app_settings = {
    APPINSIGHTS_INSTRUMENTATIONKEY = "${azurerm_application_insights.main.instrumentation_key}"
    APPINSIGHTS_PROFILERFEATURE_VERSION = "1.0.0"
    APPINSIGHTS_SNAPSHOTFEATURE_VERSION = "1.0.0"
    ApplicationInsightsAgent_EXTENSION_VERSION = "~2"
    DiagnosticServices_EXTENSION_VERSION = "~3"
    InstrumentationEngine_EXTENSION_VERSION = "~1"
    SnapshotDebugger_EXTENSION_VERSION = "~1"
    XDT_MicrosoftApplicationInsights_BaseExtensions = "~1"
    WEBSITE_TIME_ZONE = "GMT Standard Time"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_app_service" "domainapi" {
  name                    = "111-${var.locale-short}-${var.environment-short}-domain-api-${var.intnumber}"
  location                = "${var.locale}"
  resource_group_name     = "${var.resource-group}"
  app_service_plan_id     = "${azurerm_app_service_plan.main.id}"
  https_only              = "true"
  client_affinity_enabled = "false"

  site_config {
    always_on               = "true"
    min_tls_version         = "${var.tlsversion}"
  }

  app_settings = {
    APPINSIGHTS_INSTRUMENTATIONKEY = "${azurerm_application_insights.main.instrumentation_key}"
    APPINSIGHTS_PROFILERFEATURE_VERSION = "1.0.0"
    APPINSIGHTS_SNAPSHOTFEATURE_VERSION = "1.0.0"
    ApplicationInsightsAgent_EXTENSION_VERSION = "~2"
    DiagnosticServices_EXTENSION_VERSION = "~3"
    InstrumentationEngine_EXTENSION_VERSION = "~1"
    SnapshotDebugger_EXTENSION_VERSION = "~1"
    XDT_MicrosoftApplicationInsights_BaseExtensions = "~1"
    WEBSITE_TIME_ZONE = "GMT Standard Time"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_app_service" "domaindosapi" {
  name                    = "111-${var.locale-short}-${var.environment-short}-domain-dos-api-${var.intnumber}"
  location                = "${var.locale}"
  resource_group_name     = "${var.resource-group}"
  app_service_plan_id     = "${azurerm_app_service_plan.main.id}"
  https_only              = "true"
  client_affinity_enabled = "false"

  site_config {
    always_on               = "true"
    min_tls_version         = "${var.tlsversion}"
  }

  app_settings = {
    APPINSIGHTS_INSTRUMENTATIONKEY = "${azurerm_application_insights.main.instrumentation_key}"
    APPINSIGHTS_PROFILERFEATURE_VERSION = "1.0.0"
    APPINSIGHTS_SNAPSHOTFEATURE_VERSION = "1.0.0"
    ApplicationInsightsAgent_EXTENSION_VERSION = "~2"
    DiagnosticServices_EXTENSION_VERSION = "~3"
    InstrumentationEngine_EXTENSION_VERSION = "~1"
    SnapshotDebugger_EXTENSION_VERSION = "~1"
    XDT_MicrosoftApplicationInsights_BaseExtensions = "~1"
    WEBSITE_TIME_ZONE = "GMT Standard Time"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_app_service" "integrationdosapi" {
  name                    = "111-${var.locale-short}-${var.environment-short}-integration-dos-api-${var.intnumber}"
  location                = "${var.locale}"
  resource_group_name     = "${var.resource-group}"
  app_service_plan_id     = "${azurerm_app_service_plan.main.id}"
  https_only              = "true"
  client_affinity_enabled = "false"

  site_config {
    always_on               = "true"
    min_tls_version         = "${var.tlsversion}"
  }

  app_settings = {
    APPINSIGHTS_INSTRUMENTATIONKEY = "${azurerm_application_insights.main.instrumentation_key}"
    APPINSIGHTS_PROFILERFEATURE_VERSION = "1.0.0"
    APPINSIGHTS_SNAPSHOTFEATURE_VERSION = "1.0.0"
    ApplicationInsightsAgent_EXTENSION_VERSION = "~2"
    DiagnosticServices_EXTENSION_VERSION = "~3"
    InstrumentationEngine_EXTENSION_VERSION = "~1"
    SnapshotDebugger_EXTENSION_VERSION = "~1"
    XDT_MicrosoftApplicationInsights_BaseExtensions = "~1"
    WEBSITE_TIME_ZONE = "GMT Standard Time"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_app_service" "itkdispatcherapi" {
  name                    = "111-${var.locale-short}-${var.environment-short}-itk-dispatcher-api-${var.intnumber}"
  location                = "${var.locale}"
  resource_group_name     = "${var.resource-group}"
  app_service_plan_id     = "${azurerm_app_service_plan.main.id}"
  https_only              = "true"
  client_affinity_enabled = "false"

  site_config {
    always_on               = "true"
    min_tls_version         = "${var.tlsversion}"
  }

  app_settings = {
    APPINSIGHTS_INSTRUMENTATIONKEY = "${azurerm_application_insights.main.instrumentation_key}"
    APPINSIGHTS_PROFILERFEATURE_VERSION = "1.0.0"
    APPINSIGHTS_SNAPSHOTFEATURE_VERSION = "1.0.0"
    ApplicationInsightsAgent_EXTENSION_VERSION = "~2"
    DiagnosticServices_EXTENSION_VERSION = "~3"
    InstrumentationEngine_EXTENSION_VERSION = "~1"
    SnapshotDebugger_EXTENSION_VERSION = "~1"
    XDT_MicrosoftApplicationInsights_BaseExtensions = "~1"
    WEBSITE_TIME_ZONE = "GMT Standard Time"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

resource "azurerm_app_service" "feedbackapi" {
  name                    = "111-${var.locale-short}-${var.environment-short}-feedback-api-${var.intnumber}"
  location                = "${var.locale}"
  resource_group_name     = "${var.resource-group}"
  app_service_plan_id     = "${azurerm_app_service_plan.main.id}"
  https_only              = "true"
  client_affinity_enabled = "false"

  site_config {
    always_on               = "true"
    min_tls_version         = "${var.tlsversion}"
  }

  app_settings = {
    APPINSIGHTS_INSTRUMENTATIONKEY = "${azurerm_application_insights.main.instrumentation_key}"
    APPINSIGHTS_PROFILERFEATURE_VERSION = "1.0.0"
    APPINSIGHTS_SNAPSHOTFEATURE_VERSION = "1.0.0"
    ApplicationInsightsAgent_EXTENSION_VERSION = "~2"
    DiagnosticServices_EXTENSION_VERSION = "~3"
    InstrumentationEngine_EXTENSION_VERSION = "~1"
    SnapshotDebugger_EXTENSION_VERSION = "~1"
    XDT_MicrosoftApplicationInsights_BaseExtensions = "~1"
    WEBSITE_TIME_ZONE = "GMT Standard Time"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}
