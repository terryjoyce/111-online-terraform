resource "azurerm_application_insights" "main" {
  name                = "111-${var.intnumber}"
  location            = "UK South"
  resource_group_name = "${var.resource-group}"
  application_type    = "Web"

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}

output "instrumentation_key" {
  value = "${azurerm_application_insights.main.instrumentation_key}"
}
