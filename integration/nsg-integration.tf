resource "azurerm_network_security_group" "nsg-integration" {
  name                = "nsg-${var.locale-short}-${var.environment}"
  location            = "${var.locale}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  security_rule {
    name                       = "LOOPBACK"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "172.${var.address-middle}.${var.integration-address-end}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "TJ_IN"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = "${var.port-102}"
    source_address_prefix      = "${var.source-102}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "BWP_IN"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = "${var.port-103}"
    source_address_prefix      = "${var.source-103}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HEDGEEND_IN"
    priority                   = 104
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = "${var.port-104}"
    source_address_prefix      = "${var.source-104}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "PH_IN"
    priority                   = 105
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = "${var.port-105}"
    source_address_prefix      = "${var.source-105}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "GENERAL_IN"
    priority                   = 201
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = "${var.port-201}"
    source_address_prefix      = "${var.source-201}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "OCTOPUS_IN"
    priority                   = 301
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = "${var.port-301}"
    source_address_prefix      = "${var.source-301}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "BA01_IN"
    priority                   = 302
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = "${var.port-302}"
    source_address_prefix      = "${var.source-302}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "MONITORING_IN"
    priority                   = 303
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = "${var.port-303}"
    source_address_prefix      = "${var.source-303}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "BA05_IN"
    priority                   = 304
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = "${var.port-304}"
    source_address_prefix      = "${var.source-304}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "DENY_ALL"
    priority                   = 4096
    direction                  = "Inbound"
    access                     = "Deny"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    TagVersion = "0.2",
    Program = "111 Online",
    Product = "111 Online",
    Owner = "Debbie Floyd",
    TechnicalContact = "Terry Joyce",
    CostCentre = "P0436/02",
    DataClassification = "5",
    Environment = "${var.environment}",
    EnvironmentLocation = "${var.locale}",
    ServiceCategory = "Silver"
  }
}
