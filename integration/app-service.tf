# App Service CI
module "app_service_1" {
  source              = "./modules/app_service"
  intnumber           = "ci"
  tlsversion          = "1.0"
  size                = "S2"
  locale              = "${var.locale}"
  locale-short        = "${var.locale-short}"
  environment         = "${var.environment}"
  environment-short   = "${var.environment-short}"
  resource-group      = "${azurerm_resource_group.main.name}"
}
# App Service INT1
module "app_service_2" {
  source              = "./modules/app_service"
  intnumber           = "int1"
  tlsversion          = "1.0"
  size                = "S2"
  locale              = "${var.locale}"
  locale-short        = "${var.locale-short}"
  environment         = "${var.environment}"
  environment-short   = "${var.environment-short}"
  resource-group      = "${azurerm_resource_group.main.name}"
}
# App Service INT2
module "app_service_3" {
  source              = "./modules/app_service"
  intnumber           = "int2"
  tlsversion          = "1.0"
  size                = "S2"
  locale              = "${var.locale}"
  locale-short        = "${var.locale-short}"
  environment         = "${var.environment}"
  environment-short   = "${var.environment-short}"
  resource-group      = "${azurerm_resource_group.main.name}"
}
# App Service INT3
module "app_service_4" {
  source              = "./modules/app_service"
  intnumber           = "int3"
  tlsversion          = "1.0"
  size                = "S2"
  locale              = "${var.locale}"
  locale-short        = "${var.locale-short}"
  environment         = "${var.environment}"
  environment-short   = "${var.environment-short}"
  resource-group      = "${azurerm_resource_group.main.name}"
}
# App Service INT4
module "app_service_5" {
  source              = "./modules/app_service"
  intnumber           = "int4"
  tlsversion          = "1.0"
  size                = "S2"
  locale              = "${var.locale}"
  locale-short        = "${var.locale-short}"
  environment         = "${var.environment}"
  environment-short   = "${var.environment-short}"
  resource-group      = "${azurerm_resource_group.main.name}"
}
