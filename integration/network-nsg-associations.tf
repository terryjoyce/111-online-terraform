resource "azurerm_subnet_network_security_group_association" "main" {
  subnet_id                 = "${azurerm_subnet.integration.id}"
  network_security_group_id = "${azurerm_network_security_group.nsg-integration.id}"
}
