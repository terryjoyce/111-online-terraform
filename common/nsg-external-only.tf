resource "azurerm_network_security_group" "nsg-edge" {
  name                = "nsg-external-hosts"
  location            = "UK South"
  resource_group_name = "111-common"

  security_rule {
    name                       = "LOOPBACK"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "${var.source-101}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTPS_IN_OCTOPUS"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = [ "80","443" ]
    source_address_prefix      = "*"
    destination_address_prefix = "${var.destination-102}"
  }
  security_rule {
    name                       = "HTTPS_IN_TEAMCITY"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = [ "80","443" ]
    source_address_prefix      = "*"
    destination_address_prefix = "${var.destination-103}"
  }
  security_rule {
    name                       = "HTTPS_IN_NAGIOS"
    priority                   = 104
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = [ "80","443" ]
    source_address_prefix      = "*"
    destination_address_prefix = "${var.destination-104}"
  }
  security_rule {
    name                       = "HTTP_IN_SCREENGRABBER"
    priority                   = 105
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["80"]
    source_address_prefix      = "*"
    destination_address_prefix = "${var.destination-105}"
  }
  security_rule {
    name                       = "HTTP_IN_SEARCHTEST"
    priority                   = 106
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["80"]
    source_address_prefix      = "*"
    destination_address_prefix = "${var.destination-106}"
  }
  security_rule {
    name                       = "HTTP_IN_SONARQUBE"
    priority                   = 107
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["80"]
    source_address_prefix      = "*"
    destination_address_prefix = "${var.destination-107}"
  }
  security_rule {
    name                       = "ALL_IN_ES"
    priority                   = 108
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["${var.port-108}"]
    source_address_prefix      = "*"
    destination_address_prefix = "${var.destination-108}"
  }
  security_rule {
    name                       = "MONITORING_IN"
    priority                   = 109
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["${var.port-109}"]
    source_address_prefix      = "${var.source-109}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "TJ_IN"
    priority                   = 201
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["${var.port-201}"]
    source_address_prefix      = "${var.source-201}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "NHSD_IN"
    priority                   = 202
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["${var.port-202}"]
    source_address_prefix      = "${var.source-202}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "SHANE_IN"
    priority                   = 203
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["${var.port-203}"]
    source_address_prefix      = "${var.source-203}"
    destination_address_prefix = "${var.destination-203}"
  }
  security_rule {
    name                       = "JAMES_IN_SSH"
    priority                   = 204
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["${var.port-204}"]
    source_address_prefix      = "${var.source-204}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HE_IN_SSH"
    priority                   = 205
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["${var.port-205}"]
    source_address_prefix      = "${var.source-205}"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "DENY_ALL"
    priority                   = 4096
    direction                  = "Inbound"
    access                     = "Deny"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
